<?php

namespace TestLab\Controller;

use MongoDB\Driver\Manager;
use Slim\Http\Request;
use Slim\Http\Response;
use TestLab\Manager\Records;
use TestLab\Model\Error;
use TestLab\Model\Index;
use TestLab\Model\Record;

class Book
{

    public function __construct()
    {
        Records::init();
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function index($request, $response)
    {
        try {
            $index = new Index;
            $index->fromJSON($request->getBody());
            return json_encode(Records::getAll($index));
        } catch (\Exception $e) {
            $err = new Error;
            $err->error = $e->getMessage();
            return $err->getJSON();
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function show($request, $response)
    {
        try {
            $item = Records::getOne($request->getParam('id'));
            return $item->getJSON();
        } catch (\Exception $e) {
            $err = new Error;
            $err->error = $e->getMessage();
            return $err->getJSON();
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function create($request, $response)
    {
        try {
            $item = new Record;
            $item->fromJSON($request->getBody());
            $item->validate();
            Records::create($item);
            return '{}';
        } catch (\Exception $e) {
            $err = new Error;
            $err->error = $e->getMessage();
            return $err->getJSON();
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function update($request, $response)
    {
        try {
            $item = new Record;
            $item->fromJSON($request->getBody());
            $item->validate();
            Records::update($request->getParam('id'), $item);
            return '{}';
        } catch (\Exception $e) {
            $err = new Error;
            $err->error = $e->getMessage();
            return $err->getJSON();
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function delete($request, $response)
    {
        try {
            Records::delete($request->getParam('id'));
            return '{}';
        } catch (\Exception $e) {
            $err = new Error;
            $err->error = $e->getMessage();
            return $err->getJSON();
        }
    }

}
