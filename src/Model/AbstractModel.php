<?php

namespace TestLab\Model;

abstract class AbstractModel
{

    protected $dbMapping = array();

    /**
     * @return string
     */
    public function getJSON()
    {
        return json_encode($this);
    }

    /**
     * @param string $data
     */
    public function fromJSON($data)
    {
        $json = json_decode($data, TRUE);
        $vars = get_object_vars($this);
        foreach (array_keys($vars) as $var) {
            if (isset($json[$var])) {
                $this->{$var} = $json[$var];
            }
        }
    }

    /**
     * @param array $row
     */
    public function fromDB($row)
    {
        foreach ($this->dbMapping as $key => $dbKey) {
            if (isset($row[$dbKey])) {
                $this->{$key} = $row[$dbKey];
            }
        }
    }

    /**
     * @return array
     */
    public function toDB()
    {
        $row = array();
        foreach ($this->dbMapping as $key => $dbKey) {
            $row[$dbKey] = $this->{$key};
        }
        return $row;
    }

}
