<?php

namespace TestLab\Model;

/**
 * Class Record
 * @package TestLab\Models
 * Describes Record format
 */
class Record extends AbstractModel
{

    /** @var string format CC-{N}-SL where N is a number */
    public $idLine;

    /** @var string */
    public $idPref;

    /** @var int */
    public $id;

    /** @var int */
    public $idSuf;

    /** @var string */
    public $firstName;

    /** @var string */
    public $lastName;

    /** @var string */
    public $patronymicName;

    /** @var string */
    public $phone;

    /** @var string */
    public $email;

    protected $dbMapping = array(
        'idPref' => 'id_pref',
        'id' => 'id',
        'idSuf' => 'id_suf',
        'firstName' => 'first_name',
        'lastName' => 'last_name',
        'patronymicName' => 'patron_name',
        'phone' => 'phone',
        'email' => 'email',
    );

    public function fromDB($row)
    {
        parent::fromDB($row);
        $this->idLine = sprintf('%s-%s-%s', $this->idPref, str_pad($this->id, 6, '0', STR_PAD_LEFT), $this->idSuf);
    }

    public function toDB()
    {
        $row = parent::toDB();
        unset($row['id']);
        $row['id_pref'] = 'CC';
        $row['id_suf'] = 'SL';
        return $row;
    }

    public function fromJSON($data)
    {
        parent::fromJSON($data);
        $this->lastName = trim($this->lastName);
    }

    /**
     * @throws \Exception
     */
    public function validate()
    {
        if (empty($this->lastName)) {
            throw new \Exception('Last Name should not be empty');
        }
        if (!preg_match('/^\d\(\d{3}\)\d{7}$/', $this->phone)) {
            throw new \Exception('Incorrect phone format. Must be x(xxx)xxxxxxx');
        }
        if (!preg_match('/^[\w\d\-_]+@[\w\d\-_\.]+\.[\d\w]+$/', $this->email)) {
            throw new \Exception('Invalid email format');
        }
    }

}
