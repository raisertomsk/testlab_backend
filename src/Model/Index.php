<?php

namespace TestLab\Model;

/**
 * Class Index
 * @package TestLab\Models
 * Describes index request from front-end
 */
class Index extends AbstractModel
{

    /** @var int to start from */
    public $from;

    /** @var int count per page */
    public $quantity;

    /** @var string sort order */
    public $order;

    public function fromJSON($data)
    {
        parent::fromJSON($data);
        $this->from = (int) $this->from;
        if ($this->from < 0) {
            $this->from = 0;
        }
        $this->quantity = (int) $this->quantity;
        if ($this->quantity < 1) {
            $this->quantity = 10;
        }
        if (!$this->order) {
            $this->order = 'id';
        }
    }

}
