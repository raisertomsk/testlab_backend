<?php

$app->POST('/index', '\TestLab\Controller\Book:index');
$app->get('/show', '\TestLab\Controller\Book:show');
$app->post('/create', '\TestLab\Controller\Book:create');
$app->post('/update', '\TestLab\Controller\Book:update');
$app->get('/delete', '\TestLab\Controller\Book:delete');
