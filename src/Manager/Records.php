<?php

namespace TestLab\Manager;

use TestLab\Model\Index;
use TestLab\Model\Record;

class Records
{

    /** @var \PDO */
    public static $db;

    public static function init()
    {
        self::$db = new \PDO('mysql:host=localhost; dbname=testlab', 'root', '');
    }

    /**
     * @param Index $index
     * @return array
     * @throws \Exception
     */
    public static function getAll($index)
    {
        if ($index->quantity > 100) {
            $index->quantity = 100;
        }
        $items = self::fetchDataFromDB(
            'SELECT * FROM testlab.records ORDER BY :order DESC LIMIT :from, :quantity', $index
        );
        $countStatement = self::$db->prepare('SELECT COUNT(*) from testlab.records');
        $countStatement->execute();
        $count = $countStatement->fetch()[0] / $index->quantity;
        if ($count > floor($count)) {
            $count = ceil($count);
        }
        return array('response' => $items, 'pagesCount' => $count);
    }

    /**
     * @param string $id
     * @return Record
     * @throws \Exception
     */
    public static function getOne($id)
    {
        $idLine = explode('-', $id);
        if (count($idLine) != 3) {
            throw new \Exception('Incorrect id format specified');
        }
        $items = self::fetchDataFromDB(
            'SELECT * FROM testlab.records WHERE id_pref = :id_pref AND id = :id AND id_suf = :id_suf LIMIT 0, 1',
            array('id_pref' => $idLine[0], 'id' => (int) $idLine[1], 'id_suf' => $idLine[2])
        );
        if (count($items) != 1) {
            throw new \Exception('Item not found');
        }
        return $items[0];
    }

    /**
     * @param string $sql
     * @param array|Index $params
     * @return Record[]
     * @throws \Exception
     */
    private static function fetchDataFromDB($sql, $params)
    {
        $statement = self::$db->prepare($sql);
        if (is_object($params)) {
            $statement->bindParam(':from', $params->from, \PDO::PARAM_INT);
            $statement->bindParam(':quantity', $params->quantity, \PDO::PARAM_INT);
            $statement->bindParam(':order', $params->order, \PDO::PARAM_STR);
            $result = $statement->execute();
        } else {
            $result = $statement->execute($params);
        }
        if (!$result) {
            throw new \Exception($sql . ' ||| ' . $statement->errorInfo()[2]);
        }
        $items = array();
        foreach ($statement as $row) {
            $item = new Record;
            $item->fromDB($row);
            $items[] = $item;
        }
        return $items;
    }

    /**
     * @param Record $item
     */
    public static function create($item)
    {
        $row = $item->toDB();
        $statement = self::$db->prepare(sprintf(
            'INSERT INTO testlab.records(%s) VALUES (%s)',
            implode(', ', array_keys($row)), ':' . implode(', :', array_keys($row))
        ));
        $statement->execute($row);
    }

    /**
     * @param string $id
     * @param Record $item
     * @throws \Exception
     */
    public static function update($id, $item)
    {
        $id = explode('-', $id);
        if (count($id) != 3) {
            throw new \Exception('Incorrect ID passed');
        }
        $row = $item->toDB();
        $requestString = array();
        foreach (array_keys($row) as $key) {
            $requestString[] = $key . ' = :' . $key;
        }
        $statement = self::$db->prepare(sprintf(
            'UPDATE testlab.records SET %s WHERE id_pref="%s" AND id = %d AND id_suf="%s"',
            implode(', ', $requestString), $id[0], $id[1], $id[2]
        ));
        $statement->execute($row);
    }

    /**
     * @param string $id
     * @throws \Exception
     */
    public static function delete($id)
    {
        $id = explode('-', $id);
        if (count($id) != 3) {
            throw new \Exception('Incorrect ID passed');
        }
        $statement = self::$db->prepare(sprintf(
            'DELETE FROM testlab.records WHERE id_pref="%s" AND id = %d AND id_suf="%s"',
            $id[0], $id[1], $id[2]
        ));
        $statement->execute();
    }

}
